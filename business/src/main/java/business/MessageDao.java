package business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageDao {

    Logger logger = LoggerFactory.getLogger(MessageDao.class);

    private ConnectionProvider provider;

    public MessageDao() {
        provider = new ConnectionProvider();
    }


    public Message getMessage(long id) {
        Connection conn = provider.getConnection();
        try (PreparedStatement ps = conn.prepareStatement("select id,message from message where id = ?");
             ResultSet rs = getMessageQuery(ps, id);
        ) {
            if (rs.next()) {
                return new Message(rs.getLong(1), rs.getString(2));
            }
        } catch (SQLException e) {
            logger.error("getMessage() failed", e);
        }
        return null;
    }

    private ResultSet getMessageQuery(PreparedStatement ps, long id) throws SQLException {
        ps.setLong(1, id);
        return ps.executeQuery();
    }

    public Message createMessage(String message) {
        Connection conn = provider.getConnection();
        long id = getMaxId(conn) + 1;
        try (PreparedStatement ps = conn.prepareStatement("insert into message values (?,?)")) {
            ps.setLong(1, id);
            ps.setString(2, message);
            ps.execute();
            return new Message(id, message);
        } catch (SQLException e) {
            logger.error("persistMessage() failed", e);
        }
        return null;
    }

    private long getMaxId(Connection conn) {
        try (PreparedStatement ps = conn.prepareStatement("select max(id) from message");
             ResultSet rs = ps.executeQuery()) {
            rs.next();
            return rs.getLong(1);
        } catch (SQLException e) {
            logger.error("unable to get max id", e);
        }
        return 0;
    }

}
