package business;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider implements AutoCloseable {

    private Connection connection;

    public synchronized Connection getConnection() {
        if (connection == null) {
            try {
                initConnection();
            } catch (SQLException e) {
                throw new IllegalStateException("database connection could not be established", e);
            }
        }
        return connection;
    }

    private void initConnection() throws SQLException {
        Path dbPath = Paths.get("db");
        //h2 accepts only absolute paths...
        connection = DriverManager.getConnection("jdbc:h2:" + dbPath.toAbsolutePath());
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}
