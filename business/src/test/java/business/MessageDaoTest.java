package business;

import business.ConnectionProvider;
import business.Message;
import business.MessageDao;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.testng.Assert.*;

public class MessageDaoTest {

    private MessageDao messageDao;

    @BeforeClass
    public void setup() throws SQLException {
        ConnectionProvider provider = new ConnectionProvider();
        Connection connection = provider.getConnection();
        PreparedStatement ps = connection.prepareStatement("create table IF NOT EXISTS message (id bigint, message varchar2, PRIMARY KEY (id)) ");
        ps.execute();
        connection.close();
        messageDao = new MessageDao();
    }


    @Test(priority = 1)
    public void testPersistMessage() {
        Message message = messageDao.createMessage("Hello World!");
        assertEquals("Hello World!", message.getMessage());
        assertTrue(message.getId() > 0);
    }


    @Test(priority = 2)
    public void testGetMessage() {
        Message message = messageDao.getMessage(1);
        assertEquals("Hello World!", message.getMessage());
    }

}